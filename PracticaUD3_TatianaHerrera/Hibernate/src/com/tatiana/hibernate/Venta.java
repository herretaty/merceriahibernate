package com.tatiana.hibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Venta {
    private int id;
    private Double totalPagar;
    private String albaran;
    private Byte referencia;
    private List<Vendedor> vendedores;
    private List<Producto> productos;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "total_pagar")
    public Double getTotalPagar() {
        return totalPagar;
    }

    public void setTotalPagar(Double totalPagar) {
        this.totalPagar = totalPagar;
    }

    @Basic
    @Column(name = "albaran")
    public String getAlbaran() {
        return albaran;
    }

    public void setAlbaran(String albaran) {
        this.albaran = albaran;
    }

    @Basic
    @Column(name = "referencia")
    public Byte getFinalizacion() {
        return referencia;
    }

    public void setFinalizacion(Byte referencia) {
        this.referencia = referencia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Venta venta = (Venta) o;
        return id == venta.id &&
                Objects.equals(totalPagar, venta.totalPagar) &&
                Objects.equals(albaran, venta.albaran) &&
                Objects.equals(referencia, venta.referencia);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, totalPagar, albaran, referencia);
    }

    @ManyToMany(mappedBy = "ventas")
    public List<Vendedor> getVendedores() {
        return vendedores;
    }

    public void setVendedores(List<Vendedor> vendedores) {
        this.vendedores = vendedores;
    }

    @ManyToMany(mappedBy = "ventas")
    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }

    @Override
    public String toString() {
        return "Venta{" +
                "id=" + id +
                ", totalPagar=" + totalPagar +
                ", albaran='" + albaran + '\'' +
                ", cantidadVentas=" + referencia +
                '}';
    }
}
