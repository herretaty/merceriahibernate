package com.tatiana.hibernate.gui;

import com.tatiana.hibernate.Producto;
import com.tatiana.hibernate.Proveedor;
import com.tatiana.hibernate.Vendedor;
import com.tatiana.hibernate.Venta;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Tatianaherrera
 */
public class Controlador implements ActionListener,ListSelectionListener {
    private Vista vista;
    private Modelo modelo;

    /**
     * constructores de vista y modelo
     * @param vista
     * @param modelo
     */
    public Controlador(Vista vista,Modelo modelo){

        this.vista=vista;
        this.modelo=modelo;

        addActionListeners(this);
        addListSelectionListener(this);

    }

    /**
     * Añade los eventos a los controles
     * @param listener
     */


    private void addActionListeners(ActionListener listener){
        vista.conexionItem.addActionListener(listener);
        vista.salirItem.addActionListener(listener);
        vista.añadirButtonProducto.addActionListener(listener);
        vista.modificarButtonProducto.addActionListener(listener);
        vista.borrarButtonProducto.addActionListener(listener);

        vista.altaButtonProveedor.addActionListener(listener);
        vista.modificarButtonProveedor.addActionListener(listener);
        vista.borrarButtonProveedor.addActionListener(listener);

        vista.altaButtonVenta.addActionListener(listener);
        vista.modificarButtonVenta.addActionListener(listener);
        vista.borrarButtonVenta.addActionListener(listener);
        vista.btnListarVenta.addActionListener(listener);

        vista.altaButtonVendedor.addActionListener(listener);
        vista.modificarButtonVendedor.addActionListener(listener);
        vista.borrarButtonVendedor.addActionListener(listener);
    }

    /**
     * añade las listas
     * @param listener
     */
    private void addListSelectionListener(ListSelectionListener listener){
        vista.listProductos.addListSelectionListener(listener);
        vista.listVentas.addListSelectionListener(listener);
        vista.listProveedores.addListSelectionListener(listener);
        vista.listVendedores.addListSelectionListener(listener);
        vista.listVentaProducto.addListSelectionListener(listener);
        vista.listaVenta1.addListSelectionListener(listener);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        String comando=e.getActionCommand();
        switch (comando){
            case "Conectar":
                try {
                    vista.conexionItem.setEnabled(false);
                    modelo.conectar();
                    vista.setTitle("MERCERIA");
                    listarProductos(modelo.getProductos());
                    listarProveedores(modelo.getProveedores());
                    listarVentas(modelo.getVentas());
                    listarVendedor(modelo.getVendedor());
                }catch (Exception e1){
                    e1.printStackTrace();
                }
                break;
            case "Salir":
                modelo.desconectar();
                System.exit(0);
                break;
            case "Añadir Producto":
                Producto producto = new Producto();
                producto.setCodigo(vista.txtCodigoP.getText());
                producto.setOrigen(vista.txtOrigen.getText());
                producto.setPrecio(Double.parseDouble(vista.txtPrecio.getText()));
                modelo.nuevoProducto(producto);
                listarProductos(modelo.getProductos());
                break;
            case "Modificar Producto":
                Producto modificarProducto= (Producto) vista.listProductos.getSelectedValue();
                modificarProducto.setCodigo(vista.txtCodigoP.getText());
                modificarProducto.setOrigen(vista.txtOrigen.getText());
                modificarProducto.setPrecio(Double.parseDouble(vista.txtPrecio.getText()));
                modelo.modificarProducto(modificarProducto);
                listarProductos(modelo.getProductos());
                break;
            case "Borrar Producto":
                Producto borrarProducto = (Producto) vista.listProductos.getSelectedValue();
                modelo.borrarProducto(borrarProducto);
                listarProductos(modelo.getProductos());
                break;
            case "Nuevo Proveedor":
                Proveedor nuevoProveedor= new Proveedor();
                nuevoProveedor.setNombre(vista.txtnombreProveedor.getText());
                nuevoProveedor.setCodigo(vista.txtcodigo.getText());
                nuevoProveedor.setVariedadProducto(vista.txtvariedadProducto.getText());
                nuevoProveedor.setFechaOfertas(Timestamp.valueOf(vista.fechaOfertas.getDateTimePermissive()));
                modelo.nuevoProveedor(nuevoProveedor);
                listarProveedores(modelo.getProveedores());
                break;
            case "Modificar Proveedor":
                Proveedor modificarProveedor=(Proveedor) vista.listProveedores.getSelectedValue();
                modificarProveedor.setNombre(vista.txtnombreProveedor.getText());
                modificarProveedor.setCodigo(vista.txtcodigo.getText());
                modificarProveedor.setVariedadProducto(vista.txtvariedadProducto.getText());
                modificarProveedor.setFechaOfertas(Timestamp.valueOf(vista.fechaOfertas.getDateTimePermissive()));
                modelo.modificarProveedor(modificarProveedor);
                listarProveedores(modelo.getProveedores());
                break;
            case "Eliminar Proveedor":
                Proveedor borrarProveedor=(Proveedor) vista.listProveedores.getSelectedValue();
                modelo.borrarProveedor(borrarProveedor);
                listarProveedores(modelo.getProveedores());
                break;
            case "Nueva Venta":
                Venta nuevaVenta=new Venta();
                nuevaVenta.setTotalPagar(Double.valueOf(vista.txtTotalPagar.getText()));
                nuevaVenta.setAlbaran(vista.txtAlbaran.getText());
                nuevaVenta.setFinalizacion(Byte.valueOf(vista.txtReferencia.getText()));
                modelo.nuevaVenta(nuevaVenta);
                listarVentas(modelo.getVentas());

                break;
            case "Modificar Venta":
                Venta modificarVenta =(Venta) vista.listVentas.getSelectedValue();
                modificarVenta.setTotalPagar(Double.valueOf(vista.txtTotalPagar.getText()));
                modificarVenta.setAlbaran(vista.txtAlbaran.getText());
                modificarVenta.setFinalizacion(Byte.valueOf(vista.txtReferencia.getText()));
                modelo.modificarVenta(modificarVenta);
                listarVentas(modelo.getVentas());
                break;
            case "Borrar Venta":
                Venta borrarVenta=(Venta) vista.listVentas.getSelectedValue();
                modelo.borrarVenta(borrarVenta);
                listarVentas(modelo.getVentas());
                break;
            case "Nuevo Vendedor":
                Vendedor nuevoVendedor=new Vendedor();
                nuevoVendedor.setNombre(vista.txtNombreVendedor.getText());
                nuevoVendedor.setApellidos(vista.txtApellidoVendedor.getText());
                nuevoVendedor.setDireccion(vista.txtDireccionVendedor.getText());
                nuevoVendedor.setTelefono(vista.textTelefono.getText());
                modelo.nuevoVendedor(nuevoVendedor);
                listarVendedor(modelo.getVendedor());
                break;
            case "Modificar Vendedor":
                Vendedor modificarVendedor =(Vendedor) vista.listVendedores.getSelectedValue();
                modificarVendedor.setNombre(vista.txtNombreVendedor.getText());
                modificarVendedor.setApellidos(vista.txtApellidoVendedor.getText());
                modificarVendedor.setDireccion(vista.txtDireccionVendedor.getText());
                modificarVendedor.setTelefono(vista.textTelefono.getText());
                modelo.modificarVendedor(modificarVendedor);
                listarVendedor(modelo.getVendedor());
                break;
            case "Borrar Vendedor":
                Vendedor borrarVendedor=(Vendedor) vista.listVendedores.getSelectedValue();
                modelo.borrarVendedor(borrarVendedor);
                listarVendedor(modelo.getVendedor());
                break;
            case "Buscar Proveedor":
                break;
            case "Listar Ventas":
                listarVentas1(modelo.getVentas());
                break;
        }
    }

    /**
     * carga una lista de los productos
     * @param list
     */


    public void listarProductos(ArrayList<Producto> list){
        vista.dlmProducto.clear();
        for (Producto producto: list){
            vista.dlmProducto.addElement(producto);
        }
    }

    /**
     *  Carga una lista de los proveedores
     * @param proveedores
     */


    public void listarProveedores(ArrayList<Proveedor>proveedores){
        vista.dlmProveedor.clear();
        for (Proveedor proveedor: proveedores){
            vista.dlmProveedor.addElement(proveedor);
        }
    }

    /**
     * Carga una lista de los vendedores
     * @param ventaVendedor
     */


    public void listarVendedor(ArrayList<Vendedor> ventaVendedor){
        vista.dlmVendedor.clear();
        for (Vendedor vendedor: ventaVendedor){
            vista.dlmVendedor.addElement(vendedor);
        }
    }

    /**
     * Carga una lista de las ventas
     * @param list
     */


    public void listarVentas(ArrayList<Venta>list){
        vista.dlmVenta.clear();
        for (Venta venta: list){
            vista.dlmVenta.addElement(venta);
        }
    }
    public void listarVentas1(ArrayList<Venta>list){
        vista.dlmVenta.clear();
        for (Venta venta: list){
            vista.dlmVenta.addElement(venta);
        }
    }
    //public void listarVentaProducto(List<Venta> list){
        //vista.dlmVentaPro.clear();
        //for (Venta venta: list){
            //vista.dlmVentaPro.addElement(venta);
        //}

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            if (e.getSource() == vista.listProveedores) {
                Proveedor proveedorSeleccionado= (Proveedor) vista.listProveedores.getSelectedValue();
                vista.txtcodigo.setText(String.valueOf(proveedorSeleccionado.getCodigo()));
                vista.txtnombreProveedor.setText(String.valueOf(proveedorSeleccionado.getNombre()));
                vista.txtvariedadProducto.setText(String.valueOf(proveedorSeleccionado.getVariedadProducto()));
                vista.fechaOfertas.setDateTimePermissive(LocalDateTime.from(proveedorSeleccionado.getFechaOfertas().toLocalDateTime()));

            } else if (e.getSource() == vista.listProductos) {
                Producto productoSelecionado = (Producto) vista.listProductos.getSelectedValue();
                vista.txtCodigoP.setText(String.valueOf(productoSelecionado.getCodigo()));
                vista.txtOrigen.setText(String.valueOf(productoSelecionado.getOrigen()));
                vista.txtPrecio.setText(String.valueOf(productoSelecionado.getPrecio()));

            } else if (e.getSource() == vista.listVentas) {
                Venta ventaSeleccionada = (Venta) vista.listVentas.getSelectedValue();
                vista.txtTotalPagar.setText(String.valueOf(ventaSeleccionada.getTotalPagar()));
                vista.txtAlbaran.setText(String.valueOf(ventaSeleccionada.getAlbaran()));
                vista.txtReferencia.setText(String.valueOf(ventaSeleccionada.getFinalizacion()));

            } else if (e.getSource() == vista.listVendedores) {
                Vendedor vendedorSeleccionado = (Vendedor) vista.listVendedores.getSelectedValue();
                vista.txtNombreVendedor.setText(String.valueOf(vendedorSeleccionado.getNombre()));
                vista.txtApellidoVendedor.setText(String.valueOf(vendedorSeleccionado.getApellidos()));
                vista.txtDireccionVendedor.setText(String.valueOf(vendedorSeleccionado.getDireccion()));
                vista.textTelefono.setText(String.valueOf(vendedorSeleccionado.getTelefono()));
            }

        }
    }

}



