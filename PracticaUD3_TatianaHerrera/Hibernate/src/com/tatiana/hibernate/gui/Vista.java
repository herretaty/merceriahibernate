package com.tatiana.hibernate.gui;

import com.github.lgooddatepicker.components.DateTimePicker;

import javax.swing.*;
import java.awt.*;

/**
 * Clase vista
 */
public class Vista extends  JFrame{
     JTabbedPane panelPestañas;
     JPanel panelVentas;
     JButton altaButtonVenta;
     JButton modificarButtonVenta;
     JButton borrarButtonVenta;
     JTextField txtTotalPagar;
     JTextField txtAlbaran;
     JTextField txtReferencia;
     JList listVentas;
     JPanel panelVendedores;
     JButton altaButtonVendedor;
     JButton modificarButtonVendedor;
     JButton borrarButtonVendedor;
     JTextField txtNombreVendedor;
     JTextField txtApellidoVendedor;
     JTextField txtDireccionVendedor;
     JList listVendedores;
     JPanel panelProveedores;
     JTextField txtcodigo;
     JTextField txtnombreProveedor;
    DateTimePicker fechaOfertas;
     JButton altaButtonProveedor;
     JButton modificarButtonProveedor;
     JButton borrarButtonProveedor;
     JTextField txtvariedadProducto;
     JList listProveedores;
     JPanel panelProducto;
     JTextField txtCodigoP;
     JTextField txtOrigen;
     JTextField txtPrecio;
     JButton añadirButtonProducto;
     JButton modificarButtonProducto;
    JButton borrarButtonProducto;
    JList listProductos;
    private JPanel panel1;
    JTextField textTelefono;
    JButton btnListarVenta;
    JList listVentaProducto;
     JList listaVenta1;

    DefaultListModel dlmVendedor;
    DefaultListModel dlmProveedor;
    DefaultListModel dlmVenta;
    DefaultListModel dlmProducto;
    DefaultListModel dlmVentas1;
    DefaultListModel dlmVentaPro;


    JMenuItem conexionItem;
    JMenuItem salirItem;

    public Vista(){
        setTitle("MERCERIA");
        setContentPane(panel1);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
        this.setSize(new Dimension(this.getWidth()+500, this.getHeight()+300));
        setLocationRelativeTo(null);

        crearModelos();

        crearMenu();
    }
    private void crearModelos(){
        dlmVendedor= new DefaultListModel ();
        listVendedores.setModel(dlmVendedor);

        dlmProducto= new DefaultListModel ();
        listProductos.setModel(dlmProducto);

        dlmProveedor=new DefaultListModel ();
        listProveedores.setModel(dlmProveedor);

        dlmVenta=new DefaultListModel ();
        listVentas.setModel(dlmVenta);

        dlmVentas1=new DefaultListModel();
        listaVenta1.setModel(dlmVentas1);

        dlmVentaPro= new DefaultListModel();
        listVentaProducto.setModel(dlmVentaPro);
        }




    private void crearMenu(){
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");

        conexionItem = new JMenuItem("Conectar");
        conexionItem.setActionCommand("Conectar");

        salirItem = new JMenuItem("Salir");
        salirItem.setActionCommand("Salir");

        menu.add(conexionItem);
        menu.add(salirItem);
        barra.add(menu);
        setJMenuBar(barra);
    }
}
