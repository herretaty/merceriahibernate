package com.tatiana.hibernate.gui;
import com.tatiana.hibernate.*;
import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;


public class Modelo {

    SessionFactory sessionFactory;

    /**
     * metodo para desconectar
     */

    public void desconectar() {
        if(sessionFactory != null && sessionFactory.isOpen())
            sessionFactory.close();
    }

    /**
     * Metodo para conectar con la base de datos
     */

    public void conectar(){
        Configuration configuracion = new Configuration();
        configuracion.configure("hibernate.cfg.xml");

        configuracion.addAnnotatedClass(Producto.class);
        configuracion.addAnnotatedClass(Venta.class);
        configuracion.addAnnotatedClass(Vendedor.class);
        configuracion.addAnnotatedClass(Proveedor.class);
        configuracion.addAnnotatedClass(Entregas.class);


        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(
                configuracion.getProperties()).build();


        sessionFactory = configuracion.buildSessionFactory(ssr);

    }

    /**
     * Metodo para crear un nuevo producto con todos los datos
     * y los añade a una list
     * @param nuevoProducto
     */


    public void nuevoProducto(Producto nuevoProducto){
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoProducto);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Metodo para crear un nuevo proveedor con todos los datos
     * y los añade a una list
     * @param nuevoProveedor
     */

    public void nuevoProveedor(Proveedor nuevoProveedor){
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoProveedor);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Metodo para crear un nueva venta con todos los datos
     * y los añade a una list
     * @param nuevaVenta
     */


    public void nuevaVenta(Venta nuevaVenta){

        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.saveOrUpdate(nuevaVenta);
        sesion.getTransaction().commit();

        sesion.close();


    }

    /**
     * Metodo para crear un nuevo vendedor con todos los datos
     * y los añade a una list
     * @param nuevoVendedor
     */
    public void nuevoVendedor(Vendedor nuevoVendedor){
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoVendedor);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     *obtiene el listado de todos los productos
     * @return
     */


    public ArrayList<Producto> getProductos(){

        Session sesion =sessionFactory.openSession();
        Query query=sesion.createQuery("FROM Producto ");
        ArrayList<Producto>lista=(ArrayList<Producto>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * obtiene el listado de todos los proveedores
     * @return
     */
    public ArrayList<Proveedor>getProveedores(){

        Session sesion =sessionFactory.openSession();
        Query query=sesion.createQuery("FROM Proveedor ");
        ArrayList<Proveedor>lista=(ArrayList<Proveedor>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * obtiene el listado de todas las ventas
     * @return
     */

    public ArrayList<Venta>getVentas(){

        Session sesion =sessionFactory.openSession();
        Query query=sesion.createQuery("FROM Venta ");
        ArrayList<Venta>lista=(ArrayList<Venta>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * obtiene el listado de todos los vendedores
     * @return
     */


    public ArrayList<Vendedor>getVendedor(){

        Session sesion =sessionFactory.openSession();
        Query query=sesion.createQuery("FROM Vendedor ");
        ArrayList<Vendedor>listaVendedor=(ArrayList<Vendedor>)query.getResultList();
        sesion.close();
        return listaVendedor;
    }
    public ArrayList<Venta>getVentaProducto(Producto productoSeleccionado){
        Session sesion = sessionFactory.openSession();
        Query query= sesion.createQuery("FROM Venta WHERE productos=:prod");
        query.setParameter("prod",productoSeleccionado);
        ArrayList<Venta> list=(ArrayList<Venta>)query.getResultList();
        return list;
    }

    /**
     * modifica el producto seleccionado
     * @param seleccionarProducto
     */


    public void modificarProducto(Producto seleccionarProducto){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(seleccionarProducto);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * modifica la venta seleccionada
     * @param seleccionarVenta
     */


    public void modificarVenta(Venta seleccionarVenta){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(seleccionarVenta);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * modifica el vendedor seleccionado
     * @param seleccionarVendedor
     */


    public void modificarVendedor(Vendedor seleccionarVendedor){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(seleccionarVendedor);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * modifica el proveedor seleccionado
     * @param seleccionarProveedor
     */

    public void modificarProveedor(Proveedor seleccionarProveedor){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(seleccionarProveedor);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * borra el producto seleccionado
     * @param borrarProducto
     */


    public void borrarProducto(Producto borrarProducto){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(borrarProducto);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * borra el vendedor seleccionado
     * @param borrarVendedor
     */


    public void borrarVendedor(Vendedor borrarVendedor){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(borrarVendedor);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * borra la venta seleccionada
     * @param borrarVenta
     */

    public void borrarVenta(Venta borrarVenta){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(borrarVenta);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * borra el proveedor selecccionado
     * @param borrarProveedor
     */

    public void borrarProveedor(Proveedor borrarProveedor){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(borrarProveedor);
        sesion.getTransaction().commit();
        sesion.close();
    }


}

