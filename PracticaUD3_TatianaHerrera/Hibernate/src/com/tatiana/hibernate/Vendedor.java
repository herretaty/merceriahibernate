package com.tatiana.hibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Vendedor {
    private int id;
    private String nombre;
    private String apellidos;
    private String direccion;
    private String telefono;
    private List<Venta> ventas;
    private List<Entregas> entregas;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Basic
    @Column(name = "direccion")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vendedor vendedor = (Vendedor) o;
        return id == vendedor.id &&
                Objects.equals(nombre, vendedor.nombre) &&
                Objects.equals(apellidos, vendedor.apellidos) &&
                Objects.equals(direccion, vendedor.direccion) &&
                Objects.equals(telefono,vendedor.telefono);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, apellidos, direccion, telefono);
    }

    @ManyToMany
    @JoinTable(name = "vendedor_venta", catalog = "", schema = "merceriah", joinColumns = @JoinColumn(name = "id_vendedor", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_venta", referencedColumnName = "id", nullable = false))
    public List<Venta> getVentas() {
        return ventas;
    }

    public void setVentas(List<Venta> ventas) {
        this.ventas = ventas;
    }

    @OneToMany(mappedBy = "vendedor")
    public List<Entregas> getEntregas() {
        return entregas;
    }

    public void setEntregas(List<Entregas> entregas) {
        this.entregas = entregas;
    }

    @Override
    public String toString() {
        return "Vendedor{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", direccion='" + direccion + '\'' +
                ", telefono='" + telefono + '\'' +
                '}';
    }
}
