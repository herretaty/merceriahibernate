package com.tatiana.hibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Producto {
    private int id;
    private String codigo;
    private String origen;
    private double precio;
    private List<Proveedor> proveedores;
    private List<Venta> ventas;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "codigo")
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Basic
    @Column(name = "origen")
    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    @Basic
    @Column(name = "precio")
    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Producto producto = (Producto) o;
        return id == producto.id &&
                Double.compare(producto.precio, precio) == 0 &&
                Objects.equals(codigo, producto.codigo) &&
                Objects.equals(origen, producto.origen);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, codigo, origen, precio);
    }

    @ManyToMany(mappedBy = "productos")
    public List<Proveedor> getProveedores() {
        return proveedores;
    }

    public void setProveedores(List<Proveedor> proveedores) {
        this.proveedores = proveedores;
    }

    @ManyToMany
    @JoinTable(name = "venta_producto", catalog = "", schema = "merceriah", joinColumns = @JoinColumn(name = "id_producto", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_venta", referencedColumnName = "id", nullable = false))
    public List<Venta> getVentas() {
        return ventas;
    }

    public void setVentas(List<Venta> ventas) {
        this.ventas = ventas;
    }

    @Override
    public String toString() {
        return "Producto{" +
                "id=" + id +
                ", codigo='" + codigo + '\'' +
                ", origen='" + origen + '\'' +
                ", precio=" + precio +
                '}';
    }
}
