CREATE DATABASE IF NOT EXISTS merceriah;

USE merceriah;

CREATE TABLE IF NOT EXISTS vendedor (
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	nombre VARCHAR(50) NOT NULL UNIQUE,
    apellidos VARCHAR(40)NOT NULL UNIQUE,
	direccion VARCHAR(50),
	telefono VARCHAR(20)
);
CREATE TABLE IF NOT EXISTS proveedor(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	codigo VARCHAR(50) UNIQUE NOT NULL,
	nombre VARCHAR(50) NOT NULL,
    variedadProducto VARCHAR (40) NOT NULL,
	fechaOferta TIMESTAMP DEFAULT CURRENT_TIMESTAMP()
);
CREATE TABLE IF NOT EXISTS venta(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	total_pagar FLOAT DEFAULT 0,
    albaran VARCHAR(15)NOT NULL,
	referencia BOOLEAN DEFAULT FALSE
);
CREATE TABLE IF NOT EXISTS producto(
	id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
	codigo VARCHAR(10) UNIQUE NOT NULL,
	origen VARCHAR(100)NOT NULL,
	precio FLOAT DEFAULT 0
    );
    
CREATE TABLE IF NOT EXISTS vendedor_proveedor(
	id_vendedor INT UNSIGNED REFERENCES vendedor,
	id_proveedor INT UNSIGNED REFERENCES proveedor,
	fechaCompra TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
	PRIMARY KEY (id_vendedor, id_proveedor)
);
CREATE TABLE IF NOT EXISTS proveedor_producto(
	id_proveedor INT UNSIGNED REFERENCES proveedor,
	id_producto INT UNSIGNED REFERENCES producto,
	PRIMARY KEY (id_proveedor, id_producto)
);
CREATE TABLE IF NOT EXISTS venta_producto(
	id_venta INT UNSIGNED REFERENCES venta,
	id_producto INT UNSIGNED REFERENCES producto,
	PRIMARY KEY (id_venta, id_producto)
);
CREATE TABLE IF NOT exists vendedor_venta(
id_vendedor int unsigned references vendedor,
id_venta int unsigned references venta,
primary key(id_vendedor, id_Venta)
);